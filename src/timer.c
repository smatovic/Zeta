/*
  Name:         Zeta
  Description:  Experimental chess engine written in OpenCL.
  Author:       Srdja Matovic <s.matovic@app26.de>
  Created at:   2011-01-15
  Updated at:   2021-08-11
  License:      MIT

  Copyright (c) 2011-2021 Srdja Matovic

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

*/

#include <stdio.h>      // for file IO
#include <string.h>     // for string comparing functions
#include <time.h>       // for time measurement
#include <sys/time.h>   // for gettimeofday

// get time in milli seconds
double get_time(void) 
{
  struct timeval t;
  gettimeofday (&t, NULL);
  return t.tv_sec*1000 + t.tv_usec/1000;
}
void get_date_string(char *string)
{
  time_t t;
  struct tm *local;
  char *ptr;
  char tempstring[256];

  t = time(NULL);
  local = localtime (&t);
  strftime(tempstring, sizeof(tempstring), "%Y-%m-%d %H:%M:%S", local);

  if ((ptr = strchr (tempstring, '\n')) != NULL)
    *ptr = '\0';

  memcpy (string, tempstring, sizeof(char)*256);
}
void fprintdate(FILE *file)
{
  char timestring[256];
  get_date_string (timestring);
  fprintf(file, "[%s] ", timestring);
}

