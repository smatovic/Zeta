/*
  Name:         Zeta
  Description:  Experimental chess engine written in OpenCL.
  Author:       Srdja Matovic <s.matovic@app26.de>
  Created at:   2018-03-25
  Updated at:   2021-08-11
  License:      MIT

  Copyright (c) 2011-2021 Srdja Matovic

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

*/

#include "types.h"        // types and defaults and macros 

/* population count, Donald Knuth SWAR style */
/* as described on CWP */
/* https://www.chessprogramming.org/Population_Count#The_PopCount_routine */
s32 popcount(u64 x) 
{
  x =  x                        - ((x >> 1)  & 0x5555555555555555);
  x = (x & 0x3333333333333333)  + ((x >> 2)  & 0x3333333333333333);
  x = (x                        +  (x >> 4)) & 0x0f0f0f0f0f0f0f0f;
  x = (x * 0x0101010101010101) >> 56;
  return (s32)x;
}
/*  pre condition: x != 0; */
s32 first1(u64 x)
{
  return popcount((x&-x)-1);
}
/*  pre condition: x != 0; */
s32 popfirst1(u64 *a)
{
  u64 b = *a;
  *a &= (*a-1);  /* clear lsb  */
  return popcount((b&-b)-1); /* return pop count of isolated lsb */
}
/* Squares in between, pure calculation */
/* https://www.chessprogramming.org/Square_Attacked_By#Pure_Calculation */
u64 sqinbetween(Square sq1, Square sq2)
{
  const u64 m1   = 0xFFFFFFFFFFFFFFFF;
  const u64 a2a7 = 0x0001010101010100;
  const u64 b2g7 = 0x0040201008040200;
  const u64 h1b7 = 0x0002040810204080;
  u64 btwn, line, rank, file;

  btwn  = (m1 << sq1) ^ (m1 << sq2);
  file  =   (sq2 & 7) - (sq1   & 7);
  rank  =  ((sq2 | 7) -  sq1) >> 3 ;
  line  =      (   (file  &  7) - 1) & a2a7; // a2a7 if same file
  line += 2 * ((   (rank  &  7) - 1) >> 58); // b1g1 if same rank
  line += (((rank - file) & 15) - 1) & b2g7; // b2g7 if same diagonal
  line += (((rank + file) & 15) - 1) & h1b7; // h1b7 if same antidiag
  line *= btwn & -btwn; // mul acts like shift by smaller square
  return line & btwn;   // return the bits on that line inbetween
}
/* bit twiddling
  bb_work=bb_temp&-bb_temp;  // get lsb 
  bb_temp&=bb_temp-1;       // clear lsb
*/

